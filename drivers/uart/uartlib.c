#include "drivers/uart/uartlib.h"
#include "drivers/uart/uart_generic.h"
#include "drivers/logger/logger.h"
#include "evergreen/list_generic.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define UART_NOTIFY_BIT_SUCCESS 0
#define UART_NOTIFY_BIT_FAILURE 1

enum uart_request_code
{
    UART_CONFIGURE_CHIP,
    UART_REGISTER_LISTENER,
    UART_GET_CONFIG,
    UART_DATA_RECEIVED,
    UART_DATA_SENT,
    UART_WRITE_BUFFER,
    UART_FLUSH_RX
};

struct uart_request
{
    uint8_t                 num;
    enum uart_request_code  uart_request_code;
    void*                   data;
    TaskHandle_t            task_id;
};

struct uart_chip
{
    struct uart_chip_info* uart_chip_info;
    struct uart_config     uart_config;

    uint32_t               total_bytes_sent;
    uint32_t               total_bytes_received;
    bool                   configured;

    struct list_head       list;
};

struct uart_listener
{
    uint8_t num;
    uint8_t event;
    uint8_t transfer_size;
    uint8_t* data;
    void (*func)(uint8_t num, uint8_t event, uint8_t* data, uint8_t items);
    struct list_head list;
};

struct uart_tx_data
{
    uint8_t* buffer;
    uint8_t  items;
};
static void uart_listeners_task(void* params);

LIST_HEAD(uart_chips);
LIST_HEAD(uart_listeners);
static QueueHandle_t uart_service_queue;

void uartchip_add(struct uart_chip_info* uart_chip_info)
{
    struct uart_chip* uart_chip = (struct uart_chip*)pvPortMalloc(sizeof(struct uart_chip));
    if(uart_chip == NULL)
    {
        while(1);
    }

    uart_chip->uart_chip_info       = uart_chip_info;
    uart_chip->configured           = false;
    uart_chip->total_bytes_sent     = 0;
    uart_chip->total_bytes_received = 0;

    list_add(&(uart_chip->list), &uart_chips);
}

const uint8_t* const uart_get_rx_pins(uint8_t num)
{

}

const uint8_t* const uart_get_tx_pins(uint8_t num)
{

}

static uint8_t __uart_send_service_request(uint8_t num, uint8_t request_code, void* data)
{
    struct uart_request request =
    {
        .num                = num,
        .uart_request_code  = request_code,
        .task_id            = xTaskGetCurrentTaskHandle(),
        .data               = (void*)data
    };

    while(xQueueSend(uart_service_queue, (const void*)&request, portMAX_DELAY)  != pdTRUE)
        vTaskDelay(1);

    uint32_t notified_value;
    xTaskNotifyWait(UART_NOTIFY_BIT_FAILURE | UART_NOTIFY_BIT_SUCCESS,
                    ULONG_MAX,
                    &notified_value,
                    portMAX_DELAY);

    return (notified_value & UART_NOTIFY_BIT_FAILURE);
}

inline uint8_t uart_init(uint8_t id, struct uart_config* config)
{
    return __uart_send_service_request(id, UART_CONFIGURE_CHIP, (void*)config);
}

uint8_t uart_register_listener(uint8_t num, void (*listener)(uint8_t num, uint8_t event, uint8_t* data, uint8_t items))
{
    return __uart_send_service_request(num, UART_REGISTER_LISTENER, listener);
}

inline uint8_t uart_write_buffer(uint8_t num, uint8_t* data, uint8_t items)
{
    struct uart_tx_data tx_data =
    {
        .buffer = data,
        .items  = items
    };

    return __uart_send_service_request(num, UART_WRITE_BUFFER, (void*)&tx_data);
}

static struct uart_chip* __uart_find_chip(uint8_t num)
{
    struct uart_chip* uart_chip;
    list_for_each_entry(uart_chip, &uart_chips, list)
    {
        if(uart_chip->uart_chip_info->id == num)
            return uart_chip;
    }

    return NULL;
}

static uint8_t __uart_init(uint8_t num, struct uart_config* config)
{
    struct uart_chip* uart_chip = __uart_find_chip(num);
    if(uart_chip == NULL)
        return UART_NOTIFY_BIT_FAILURE;

    if(uart_chip->configured  == true)
        return UART_NOTIFY_BIT_FAILURE;

    uint8_t temp = 0;
    while(temp < uart_chip->uart_chip_info->total_rx_pins)
    {
        if(config->rx_pin == uart_chip->uart_chip_info->rx_pins[temp])
            break;
        temp++;
    }
    if(temp == uart_chip->uart_chip_info->total_rx_pins)
        return UART_NOTIFY_BIT_FAILURE;

    temp = 0;
    while(temp < uart_chip->uart_chip_info->total_tx_pins)
    {
        if(config->tx_pin == uart_chip->uart_chip_info->tx_pins[temp])
            break;
        temp++;
    }
    if(temp == uart_chip->uart_chip_info->total_tx_pins)
        return UART_NOTIFY_BIT_FAILURE;

    uart_chip->uart_chip_info->uart_init(num, config);
    uart_chip->uart_config = *config;
    uart_chip->configured  = true;

    return UART_NOTIFY_BIT_SUCCESS;
}

static uint8_t __uart_register_listener(uint8_t num, void (*func)(uint8_t num, uint8_t event, uint8_t* data, uint8_t items))
{
    struct uart_chip* chip = __uart_find_chip(num);
    if(chip == NULL || func == NULL)
        return UART_NOTIFY_BIT_FAILURE;

    struct uart_listener* new_listener = (struct uart_listener*)pvPortMalloc(sizeof(struct uart_listener));
    if(new_listener == NULL)
        return UART_NOTIFY_BIT_FAILURE;

    new_listener->num           = num;
    new_listener->transfer_size = chip->uart_chip_info->transfer_size;
    new_listener->func          = func;

    list_add(&(new_listener->list), &uart_listeners);
    return UART_NOTIFY_BIT_SUCCESS;
}

static uint8_t __uart_write_buffer(uint8_t num, struct uart_tx_data* data)
{
    struct uart_chip* chip = __uart_find_chip(num);
    if(chip == NULL || chip->configured == false || data == NULL || data->buffer == NULL)
        return UART_NOTIFY_BIT_FAILURE;

    return chip->uart_chip_info->uart_write_buffer(num, data->buffer, data->items);
}

static void __uart_activate_listeners(uint8_t num, uint8_t event, uint8_t* buffer)
{
    struct uart_listener* listener;

    list_for_each_entry(listener, &uart_listeners, list)
    {
        if(listener->num == num)
        {
            if(event == UART_DATA_RECEIVED)
            {
                uint8_t* data = (uint8_t*)pvPortMalloc(sizeof(uint8_t) * listener->transfer_size);
                memcpy((void*)data, (void*)buffer, listener->transfer_size);
                listener->data = data;
                listener->event = UART_RX_EVENT;
            }
            else
            {
                /*listener->data = NULL;
               */
                listener->data  = buffer;
                listener->event = UART_TX_EVENT;
            }

            xTaskCreate(uart_listeners_task, "", configMINIMAL_STACK_SIZE, (void*)listener, tskIDLE_PRIORITY, NULL);
        }
    }
}

static void uart_service(void* params)
{
    struct uart_request request;
    uint8_t result;
    uint8_t i;
    uint8_t length;

    while(true)
    {
        while(xQueueReceive(uart_service_queue, (void*)&request, portMAX_DELAY) != pdTRUE)
        {
            vTaskDelay(1);
        }

        switch(request.uart_request_code)
        {
        case UART_CONFIGURE_CHIP:
            result = __uart_init(request.num, (struct uart_config*)(request.data));
            break;
        case UART_WRITE_BUFFER:
            result = __uart_write_buffer(request.num, (struct uart_tx_data*)(request.data));
            break;
        case UART_REGISTER_LISTENER:
            result = __uart_register_listener(request.num, request.data);
            break;
        case UART_DATA_RECEIVED:
        case UART_DATA_SENT:
            __uart_activate_listeners(request.num, request.uart_request_code, request.data);
            continue;
        }

        xTaskNotify(request.task_id, (uint32_t)result, eSetValueWithOverwrite);
    }
}

void uart_service_init(void)
{
    uart_module_init();
    uart_service_queue = xQueueCreate(UART_QUEUE_SIZE, sizeof(struct uart_request));
    xTaskCreate(uart_service, "", configMINIMAL_STACK_SIZE, NULL, configMAX_PRIORITIES - 1, NULL);
}

 void uart_notify_data_received(uint8_t num, uint8_t* address)
{
    struct uart_request request =
    {
        .num                = num,
        .uart_request_code  = UART_DATA_RECEIVED,
        .data               = (void*)address
    };

    xQueueSendToBackFromISR(uart_service_queue, (const void*)&request, NULL);
}

void uart_notify_data_sent(uint8_t num, uint8_t* data)
{
    struct uart_request request =
    {
        .num                = num,
        .uart_request_code  = UART_DATA_SENT,
        .data               = (void*)data
    };

    xQueueSendToBackFromISR(uart_service_queue, (const void*)&request, NULL);
}

static void uart_listeners_task(void* params)
{
    struct uart_listener* listener = (struct uart_listener*)params;
    listener->func(listener->num, listener->event, listener->data, listener->transfer_size);
    vTaskDelete(NULL);
}
