/*   * * * P R O P R I E T A R Y   A N D  C O N F I D E N T I A L * * *
 *   Author: Sava Jakovljev
 *   Date: March 2016
 *
 *   ALL RIGHT RESERVED.
 *
 *
 *   Part of Evergreen framework.
 *   uartlib.c: contains platform independent code for manipulating UART chips.
 *   Evergreen system service starts at boot time, serving user requests and
 *   manipulating hardware below.
 *   USER SHOULD ONLY USE GLOBAL FUNCTIONS FROM THIS MODULE.
 * */

#ifndef _UARTLIB_H_INCLUDED
#define _UARTLIB_H_INCLUDED

#include "drivers/uart/uart_generic.h"
#include <stdint.h>
#include <stdbool.h>

#define UART_QUEUE_SIZE     8

#define UART_RESULT_SUCCESS 0
#define UART_RESULT_FAILURE 1

#define UART_BAUD_600       600
#define UART_BAUD_9600      9600
#define UART_BAUD_57600     57600
#define UART_BAUD_115200    115200

#define UART_PARITY_NONE    0
#define UART_PARITY_EVEN    6
#define UART_PARITY_ODD     2

#define UART_STOP_BITS_ONE  1
#define UART_STOP_BITS_TWO  2

#define UART_RX_EVENT       1
#define UART_TX_EVENT       2

#define UART_LOG_TAG        "UART service"

struct uart_config
{
    uint32_t    baud_rate;
    uint8_t     parity_mode;
    uint8_t     stop_bits;
    uint8_t     tx_pin;
    uint8_t     rx_pin;
};

void uart_service_init(void);
void uartchip_add(struct uart_chip_info* uart_chip_info);
void uartchip_remove(struct uart_chip_info* uart_chip_info);

const uint8_t* const uart_get_rx_pins(uint8_t num);
const uint8_t* const uart_get_tx_pins(uint8_t num);

uint8_t uart_init(uint8_t id, struct uart_config* config);
uint8_t uart_write_buffer(uint8_t num, uint8_t* data, uint8_t items);
uint8_t uart_register_listener(uint8_t num, void (*listener)(uint8_t num, uint8_t event, uint8_t* data, uint8_t items));


#endif
