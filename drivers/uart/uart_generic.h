#ifndef _UART_GENERIC_H_INCLUDED
#define _UART_GENERIC_H_INCLUDED

#include <stdint.h>
#include <stdio.h>


struct uart_chip_info
{
    uint8_t (*uart_init)(uint8_t num, struct uart_config* config);
    uint8_t (*uart_write_buffer)(uint8_t num, uint8_t* buffer, uint8_t items);
    void    (*uart_flush_rx)(uint8_t num);

    char*       label;

    const uint8_t* const    rx_pins;
    const uint8_t* const    tx_pins;
    uint8_t                 total_rx_pins;
    uint8_t                 total_tx_pins;
    uint8_t                 transfer_size;
    uint8_t                 id;
};

void        uart_module_init(void);
extern void uartchip_add(struct uart_chip_info* uart_chip_info);
extern void uart_notify_data_received(uint8_t num, uint8_t* data);
extern void uart_notify_data_sent(uint8_t num, uint8_t* data);

#endif
