#ifndef _LOGGER_H_INCLUDED
#define _LOGGER_H_INCLUDED

#include "evergreen/list_generic.h"

#define LOGGER_RING_SIZE_BUFFER_SIZE    10

/*
#define LOGGER_OUTPUT_PERIPH_NAME    "UART"
#define LOGGER_OUTPUT_PERIPH_NUM     6
#define LOGGER_OUTPUT_TX_PIN         119
#define LOGGER_OUTPUT_RX_PIN         118
#define LOGGER_OUTPUT_PERIPH_SPEED   115200
*/

#define LOGGER_OUTPUT_PERIPH_NAME    "UART"

#ifdef PART_TM4C1294NCPDT
#define LOGGER_OUTPUT_PERIPH_NUM     0
#define LOGGER_OUTPUT_TX_PIN         1
#define LOGGER_OUTPUT_RX_PIN         0
#endif

#ifdef PART_CC3200
#define LOGGER_OUTPUT_PERIPH_NUM     0
#define LOGGER_OUTPUT_TX_PIN         55
#define LOGGER_OUTPUT_RX_PIN         57
#endif
#define LOGGER_OUTPUT_PERIPH_SPEED   115200

#define INFO_STRING     "I/"
//#define DEBUG_STRING    "D/"
#define WARNING_STRING  "W/"
#define ERROR_STRING    "E/"
#define FATAL_STRING    "F/"

enum log_level
{
    INFO,
    LOG_DEBUG,
    WARNING,
    ERROR,
    FATAL
};

void logger_init(void);
void debug(const char* const tag, const char* const message, ...);
void warning(const char* const tag, const char* const message, ...);
void info(const char* const tag, const char* const message, ...);
void error(const char* const tag, const char* const message, ...);
void fatal(const char* const tag, const char* const message, ...);

#endif
