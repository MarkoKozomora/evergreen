#include "drivers/logger/logger.h"
#include "evergreen/tinyprintf.h"
#include "evergreen/string_utils.h"

#//if LOGGER_OUTPUT_PERIPH_NAME == "UART"
#include "drivers/uart/uartlib.h"
#include "config/evergreen.h"
//#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#define BUFFER_START_SIZE 20

#define LOGGER_BIT_DATA_SENT    (1 << 2)

const char DEBUG_STRING[] = "D/";

static QueueHandle_t      log_service_queue;
static TaskHandle_t       logger_thread;
static SemaphoreHandle_t  logger_mutex;

struct log_entry
{
    char* level;
    char* tag;
    char* message;
};

static void logger_callback(uint8_t per_num, uint8_t event, uint8_t* buffer, uint8_t items)
{
    /* This should never happen. */
    if(per_num !=  LOGGER_OUTPUT_PERIPH_NUM || event != UART_TX_EVENT)
        return;

    xTaskNotify(logger_thread, LOGGER_BIT_DATA_SENT, eSetValueWithOverwrite);
}

static void log_print(char* data)
{
    uint32_t temp = strlen(data) + 1;
    while(uart_write_buffer(LOGGER_OUTPUT_PERIPH_NUM, data, temp) != 0)
        vTaskDelay(2);

    while(xTaskNotifyWait(LOGGER_BIT_DATA_SENT, ULONG_MAX, &temp, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    vTaskDelay(1);
}

static void logger_service(void* params)
{
    uint8_t count;
    struct log_entry log_entry;

    struct uart_config uart_config =
    {
        .baud_rate      = LOGGER_OUTPUT_PERIPH_SPEED,
        .parity_mode    = UART_PARITY_NONE,
        .stop_bits      = UART_STOP_BITS_ONE,
        .tx_pin         = LOGGER_OUTPUT_TX_PIN,
        .rx_pin         = LOGGER_OUTPUT_RX_PIN
    };
    uart_init(LOGGER_OUTPUT_PERIPH_NUM, &uart_config);
    uart_register_listener(LOGGER_OUTPUT_PERIPH_NUM, logger_callback);

    vTaskDelay(5);

    char* msg;
    while(true)
    {
        while(xQueueReceive(log_service_queue, (void*)&log_entry, portMAX_DELAY) != pdTRUE)
            vTaskDelay(1);
        msg = string_format("%s%s   %s\n", log_entry.level, log_entry.tag, log_entry.message);
        vPortFree(log_entry.message);
        log_print(msg);
        vTaskDelay(2);
        vPortFree(msg);
    }
}

void logger_init(void)
{
    logger_mutex = xSemaphoreCreateMutex();
    log_service_queue = xQueueCreate(15, sizeof(struct log_entry));
    if(logger_mutex == NULL || log_service_queue == NULL || xTaskCreate(logger_service, "", 220, NULL, tskIDLE_PRIORITY, &logger_thread) != pdPASS)
        system_reset();
}

void debug(const char* const tag, const char* const message, ...)
{
#if (EVERGREEN_LOGGER_ENABLED == 1)
    while(xSemaphoreTake(logger_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    va_list list;
    uint16_t max_size = BUFFER_START_SIZE;
    int8_t status;
    char* buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));

    while(true)
    {
        if(buffer == NULL)
            goto exit;

        va_start(list, message);
        status = tfp_vsnprintf(buffer, max_size, message, list);
        va_end(list);

        if(status > -1 && status < max_size)
            break;

        max_size *= 2;
        vPortFree(buffer);
        buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));
    }

    struct log_entry log_entry =
    {
        .level      = DEBUG_STRING,
        .tag        = tag,
        .message    = buffer
    };

    while(xQueueSend(log_service_queue, (void*)&log_entry, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);
exit:
    xSemaphoreGive(logger_mutex);
#endif
}

void warning(const char* const tag, const char* const message, ...)
{
#if (EVERGREEN_LOGGER_ENABLED == 1)
    while(xSemaphoreTake(logger_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    va_list list;
    uint16_t max_size = BUFFER_START_SIZE;
    int8_t status;
    char* buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));

    while(true)
    {
        if(buffer == NULL)
            return;

        va_start(list, message);
        status = tfp_vsnprintf(buffer, max_size, message, list);
        va_end(list);

        if(status > -1 && status < max_size)
            break;

        max_size *= 2;
        vPortFree(buffer);
        buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));
    }

    struct log_entry log_entry =
    {
        .level      = WARNING_STRING,
        .tag        = tag,
        .message    = buffer
    };

    while(xQueueSend(log_service_queue, (void*)&log_entry, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);
    xSemaphoreGive(logger_mutex);
#endif
}

void error(const char* const tag, const char* const message, ...)
{
#if (EVERGREEN_LOGGER_ENABLED == 1)
    while(xSemaphoreTake(logger_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    va_list list;
    uint16_t max_size = BUFFER_START_SIZE;
    int8_t status;
    char* buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));

    while(true)
    {
        if(buffer == NULL)
            return;

        va_start(list, message);
        status = tfp_vsnprintf(buffer, max_size, message, list);
        va_end(list);

        if(status > -1 && status < max_size)
            break;

        max_size *= 2;
        vPortFree(buffer);
        buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));
    }

    struct log_entry log_entry =
    {
        .level      = ERROR_STRING,
        .tag        = tag,
        .message    = buffer
    };

    while(xQueueSend(log_service_queue, (void*)&log_entry, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    xSemaphoreGive(logger_mutex);
#endif
}

void fatal(const char* const tag, const char* const message, ...)
{
#if (EVERGREEN_LOGGER_ENABLED == 1)
    while(xSemaphoreTake(logger_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    va_list list;
    uint16_t max_size = BUFFER_START_SIZE;
    int8_t status;
    char* buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));

    while(true)
    {
        if(buffer == NULL)
            return;

        va_start(list, message);
        status = tfp_vsnprintf(buffer, max_size, message, list);
        va_end(list);

        if(status > -1 && status < max_size)
            break;

        max_size *= 2;
        vPortFree(buffer);
        buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));
    }

    struct log_entry log_entry =
    {
        .level      = FATAL_STRING,
        .tag        = tag,
        .message    = buffer
    };

    while(xQueueSend(log_service_queue, (void*)&log_entry, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    xSemaphoreGive(logger_mutex);
#endif
}

void info(const char* const tag, const char* const message, ...)
{
#if (EVERGREEN_LOGGER_ENABLED == 1)
    while(xSemaphoreTake(logger_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    va_list list;
    uint16_t max_size = BUFFER_START_SIZE;
    int8_t status;
    char* buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));

    while(true)
    {
        if(buffer == NULL)
            return;

        va_start(list, message);
        status = tfp_vsnprintf(buffer, max_size, message, list);
        va_end(list);

        if(status > -1 && status < max_size)
            break;

        max_size *= 2;
        vPortFree(buffer);
        buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));
    }

    struct log_entry log_entry =
    {
        .level      = INFO_STRING,
        .tag        = tag,
        .message    = buffer
    };

    while(xQueueSend(log_service_queue, (void*)&log_entry, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    xSemaphoreGive(logger_mutex);
#endif
}
