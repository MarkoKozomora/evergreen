#ifndef _CANBUS_GENERIC_H_INCLUDED
#define _CANBUS_GENERIC_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

struct canbus_chip_info
{
    uint8_t (*canbus_init)(uint8_t num, struct canbus_config* config);
    uint8_t (*canbus_write)(uint8_t num, struct canbus_message_object* msg);

    const char* const       label;
    const uint8_t* const    rx_pins;
    const uint8_t* const    tx_pins;

    uint8_t                 total_rx_pins;
    uint8_t                 total_tx_pins;

    uint8_t                 id;
};

void        canbus_module_init(void);
extern void canbuschip_add(struct canbus_chip_info* canbus_chip_info);

#endif
