#ifndef _CANBUSLIB_H_INCLUDED
#define _CANBUSLIB_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>
#include "drivers/canbus/canbus_generic.h"

struct canbus_config
{
    uint32_t bitrate;
    uint8_t  rx_pin;
    uint8_t  tx_pin;
};

struct canbus_message_object
{
    uint32_t id;
    uint32_t mask;
    uint32_t items;
    uint8_t* buffer;
};

void canbuschip_add(struct canbus_chip_info* canbus_chip_info);

uint8_t canbus_init(uint8_t id, struct canbus_config* config);
uint8_t canbus_write_data(uint8_t id, struct canbus_message_object* msg);
#endif
