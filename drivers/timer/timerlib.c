#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>

#include "drivers/timer/timerlib.h"
#include "drivers/logger/logger.h"
#include "drivers/hooks/hooks.h"
#include "evergreen/list_generic.h"
#include "kernel/boot.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define LOG_TAG                     "Timer service"

#define TIMER_NOTIFY_BIT_SUCCESS    (1 << 1)
#define TIMER_NOTIFY_BIT_FAILURE    (1 << 2)

enum timer_request_type
{
    TIMER_CANCEL,
    TIMER_CREATE_ONE_SHOT,
    TIMER_CREATE_REPEATING,
    TIMER_ACTIVATE
};

struct timer_service_request
{
    uint8_t         id;
    uint8_t         type;
    uint32_t        period;
    TaskHandle_t    calling_task_id;
    void            (*callback)(uint8_t id, uint8_t num);
};

struct timer
{
    uint8_t          id;
    uint8_t          type;
    uint8_t          skips;
    uint32_t         last_wake_time;
    uint32_t         period;
    void             (*callback)(uint8_t id, uint8_t num);

    struct list_head list;
};

LIST_HEAD(timers);
static QueueHandle_t                 timer_service_queue;
static TickType_t                    activate_time;

static uint8_t __timer_send_request(uint8_t id, uint32_t period, uint8_t type,
                                    void (*callback)(uint8_t id, uint8_t num))
{
    struct timer_service_request* req = (struct timer_service_request*)
                                        pvPortMalloc(sizeof(struct timer_service_request));
    if(req == NULL)
        return TIMER_NOTIFY_BIT_FAILURE;

    req->id              = id;
    req->period          = period;
    req->type            = type;
    req->callback        = callback;
    req->calling_task_id = xTaskGetCurrentTaskHandle();
    while(xQueueSend(timer_service_queue, (const void*)(&req), portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    uint32_t notified_value;
    xTaskNotifyWait(TIMER_NOTIFY_BIT_FAILURE | TIMER_NOTIFY_BIT_SUCCESS,
                    ULONG_MAX,
                    &notified_value,
                    portMAX_DELAY);
    return (notified_value & TIMER_NOTIFY_BIT_FAILURE);
}

uint8_t timer_start_oneshot(uint8_t id, uint32_t period, void (*callback)(uint8_t id, uint8_t skips))
{
    return __timer_send_request(id, period, TIMER_CREATE_ONE_SHOT, callback);
}

uint8_t timer_start_repeating(uint8_t id, uint32_t period, void (*callback)(uint8_t id, uint8_t skips))
{
    return __timer_send_request(id, period, TIMER_CREATE_REPEATING, callback);
}

uint8_t timer_cancel(uint8_t id)
{
    return __timer_send_request(id, 0, TIMER_CANCEL, NULL);
}

uint8_t  __timer_start(uint8_t id, uint8_t type, uint32_t period, void (*callback)(uint8_t id, uint8_t skips))
{
    struct timer* timer = (struct timer*)pvPortMalloc(sizeof(struct timer));
    if(timer == NULL)
        return TIMER_NOTIFY_BIT_FAILURE;

    timer->id               = id;
    timer->type             = type;
    timer->period           = period;
    timer->skips            = 0;
    timer->callback         = callback;
    timer->last_wake_time   = xTaskGetTickCount();
    list_add(&(timer->list), &timers);

    return TIMER_NOTIFY_BIT_SUCCESS;
}

uint8_t __timer_cancel(uint8_t id)
{
    struct timer* timer;
    list_for_each_entry(timer, &timers, list)
    {
        if(timer->id == id)
        {
            list_del(&(timer->list));
            vPortFree(timer);

            return TIMER_NOTIFY_BIT_SUCCESS;
        }
    }

    return TIMER_NOTIFY_BIT_FAILURE;
}

static uint32_t __timer_get_next_wake_time(void)
{
    struct timer* timer;
    int32_t      temp;
    uint32_t      min_period    = 0xFFFF;
    TickType_t    current_ticks = xTaskGetTickCount();
    list_for_each_entry(timer, &timers, list)
    {
        temp = current_ticks - timer->last_wake_time;
        if(temp < min_period)
        {
            min_period = temp > 0 ? temp : 1;
        }
    }

    return (min_period == 0xFFFF ? portMAX_DELAY : min_period);
}

void __timer_activate(void)
{
    struct timer* timer;
    TickType_t    time = xTaskGetTickCount();

    list_for_each_entry(timer, &timers, list)
    {
        if(time >= timer->last_wake_time)
        {
            timer->callback(timer->id, timer->skips++);
            timer->last_wake_time = time;
            if(timer->type == TIMER_CREATE_ONE_SHOT)
            {
                list_del(&timer->list);
                vPortFree(timer);
            }
        }
    }
}

void timer_service(void* params)
{
    struct timer_service_request* req;
    uint32_t result;
    uint32_t wait_period;

    while(true)
    {
        wait_period = __timer_get_next_wake_time();
        if(xQueueReceive(timer_service_queue, (void*)&req, wait_period) == pdTRUE)
        {
            switch(req->type)
            {
            case TIMER_CANCEL:
                result = __timer_cancel(req->id);
                break;
            case TIMER_CREATE_ONE_SHOT:
            case TIMER_CREATE_REPEATING:
                result = __timer_start(req->id, req->type, req->period, req->callback);
                break;
            }
            xTaskNotify(req->calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
            vPortFree(req);
        }
        else
        {
           __timer_activate();
        }
    }
}

void timer_service_init(void)
{
    timer_service_queue = xQueueCreate(4, sizeof(struct timer_service_request*));
    if(timer_service_queue == NULL)
        system_reset();
    if(xTaskCreate(timer_service, "", 150, NULL, configMAX_PRIORITIES - 1, NULL) != pdPASS)
        system_reset();
}
