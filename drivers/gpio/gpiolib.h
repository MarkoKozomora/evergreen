/*   * * * P R O P R I E T A R Y   A N D  C O N F I D E N T I A L * * *
 *   Author: Sava Jakovljev
 *   Date: March 2016
 *
 *   ALL RIGHT RESERVED.
 *
 * */
#ifndef _GPIOLIB_H_INCLUDED
#define _GPIOLIB_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>
#include "drivers/gpio/gpio_generic.h"

#define GPIO_QUEUE_SIZE             25

#define GPIO_DIR_INPUT              1
#define GPIO_DIR_OUTPUT             2

#define GPIO_LONG_PRESS_TIME_SEC    3

#define GPIO_DEBOUNCE_COUNT         3


#define GPIO_EVENT_INPUT_ACTIVATED              5
#define GPIO_EVENT_INPUT_LONG_PRESS_ACTIVATED   6
#define GPIO_EVENT_INPUT_RELEASED               7

/*
 * Function name: gpio_service_init
 * For internal use. Used by Evergreen on kernel boot.
 * */
void gpio_service_init(void);

/*
 * Function name: gpiochip_add
 * This function should be used to add platform specific GPIO chip to
 * the Evergreen system.
 * */
uint8_t gpiochip_add(struct gpio_chip_info* gpio_chip_info);

/*
 * Function name: gpiochip_remove
 * This function should be used to remove platform specific GPIO chip from
 * the Evergreen system.
 * */
uint8_t gpiochip_remove(struct gpio_chip_info* gpio_chip_info);
//uint8_t gpiochip_pin_is_valid(uint8_t pin);

/*
 * Function name: gpio_register_pin
 * Registers a GPIO pin in the Evergreen system.
 * Parameters:
 *  - pin: meaning is platform specific. Evergreen doesn't care about anything
 *  except this number. This way Hardware Abstraction Layer(HAL) and portability
 *  is implemented.
 *  - direction: can be GPIO_DIR_INPUT or GPIO_DIR_OUTPUT
 *  - pulled_up: only important if registering pin as an input. Otherwise,
 *  this parameter is ignored.
 * Returns: 0 on success
 * Failure can occurr if platform can't map pin number to architecture
 * specific ports and bits, in other words, if pin does not exist.
 * */
uint8_t gpio_register_pin(uint8_t pin, uint8_t direction, bool pulled_up);

/*
 * Function name: gpio_write_pin
 * Sets or resets GPIO pin.
 * Parameters:
 *  - pin: number used to represent a pin in the Evergreen system.
 *  - value: value to set.
 * Returns: 0 on success.
 * If a pin has not been registered as an input, non-zero value will be returned.
 * */
uint8_t gpio_write_pin(uint8_t pin, bool value);

/*
 * Function name: gpio_read_pin
 * Returns last reported pin value.
 * Parameters:
 *  - pin: number used to represent a pin in the Evergreen system.
 * Returns: pin status, can be 0 or 1.
 * If a pin has not been registered as an input, value greater than 1 is
 * returned.
 * */
uint8_t gpio_read_pin(uint8_t pin);

/*
 * Function name: gpio_register_listener
 * Registers a listener(callback) tied to specific input pin.
 * Parameters:
 *  - pin: number used to represent a pin in the Evergreen system.
 *  - listener: function which will be called when GPIO event occurrs for the
 *  specified pin.
 * Returns: 0 on success.
 * If a pin has not been registered as an input, non-zero value is returned.
 * */
uint8_t gpio_register_listener(uint8_t pin, void (*listener)(uint8_t pin, uint8_t event));

/*
 * Function name: gpio_unregister_listener
 * Unregisters a listener(callback) tied to specific input pin.
 * Parameters:
 *  - pin: number used to represent a pin in the Evergreen system.
 *  - listener: function which has been registerd for a specific pin by calling a
 *  gpio_register_listener() function.
 * Returns: 0 on success.
 * If a pin has not been registered as an input or listener does
 * not exist, non-zero value is returned.
 * */
uint8_t gpio_unregister_listener(uint8_t pin, void (*listener)(uint8_t pin, uint8_t event));

#endif
