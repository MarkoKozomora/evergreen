#include "drivers/canbus/canbus_generic.h"
#include "drivers/canbus/canbuslib.h"
#include "drivers/logger/logger.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_can.h"
#include "driverlib/debug.h"
#include "driverlib/can.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"
#include "driverlib/gpio.h"

#define TOTAL_CANBUS_CHIPS  2

#define CANBUS_TX_MOB_ID    0

static const base[] =
{
    CAN0_BASE,
    CAN1_BASE,

    GPIO_PORTA_BASE,
    GPIO_PORTB_BASE
};

static const sysctl[] =
{
    SYSCTL_PERIPH_CAN0,
    SYSCTL_PERIPH_CAN1,

    SYSCTL_PERIPH_GPIOA,
    SYSCTL_PERIPH_GPIOB
};

static const int_base[] =
{
    INT_CAN0,
    INT_CAN1
};

static void __enable_clock(uint8_t num)
{
    if(!SysCtlPeripheralReady(sysctl[num]))
    {
        SysCtlPeripheralReset(sysctl[num]);
        SysCtlPeripheralEnable(sysctl[num]);
        while(!SysCtlPeripheralReady(sysctl[num]));
    }
}

static uint8_t canbus_config(uint8_t num, struct canbus_config* config)
{
    /* Enable clock on GPIO port */
    __enable_clock(num + 2);

    /* CAN RX pin */
    uint64_t temp = 0x07 | (num << 16);
    GPIOPinConfigure(temp);

    /* CAN TX pin */
    temp = 0x07 | (0x04 << 8) | (num << 16);
    GPIOPinConfigure(temp);

    GPIOPinTypeCAN(base[num + 2], 0x01 | 0x02);

    __enable_clock(num);
    CANInit(base[num]);
    CANBitRateSet(base[num], SysCtlClockGet(), 50000);

    //HWREG(base[num] + CAN_O_CTL) |= CAN_CTL_TEST;
    //HWREG(base[num] + CAN_O_TST) |= CAN_TST_LBACK;

    CANIntEnable(base[num], CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);
    CANIntClear(base[num], CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);

    IntEnable(int_base[num]);
    IntMasterEnable();
    CANEnable(base[num]);

    vTaskDelay(20);

    tCANMsgObject msg;

    msg.ui32MsgID = 0;
    msg.ui32MsgIDMask = 0xFFFFFFFF;
    CANMessageSet(base[num], 3, &msg, MSG_OBJ_TYPE_RX);
}

static uint8_t canbus_write(uint8_t num, struct canbus_message_object* data)
{
    tCANMsgObject msg;

    msg.ui32MsgID = data->id;
    msg.ui32MsgIDMask = data->mask;
    msg.ui32Flags = MSG_OBJ_TX_INT_ENABLE;
    msg.ui32MsgLen = data->items;
    msg.pui8MsgData = data->buffer;

    CANMessageSet(base[num], CANBUS_TX_MOB_ID, &msg, MSG_OBJ_TYPE_TX);
    vTaskDelay(200);

    return 0;
}

void canbus_isr_handler(void)
{
    volatile uint8_t i;
    volatile uint32_t status;

    for(i = 0; i < TOTAL_CANBUS_CHIPS; i++)
    {
        if(!SysCtlPeripheralReady(sysctl[i]))
            continue;

        status = CANIntStatus(base[i], CAN_INT_STS_CAUSE);
        if(status == CAN_INT_INTID_STATUS)
        {
            status = CANStatusGet(base[i], CAN_STS_CONTROL);
            CANIntClear(base[i], status);
            //CANIntDisable(base[i], status);
            return;
        }

        CANIntClear(base[i], status);
    }
}

static struct canbus_chip_info canbus1 =
{
    .canbus_init    = canbus_config,
    .canbus_write   = canbus_write,

    .id = 0
};

static struct canbus_chip_info canbus2 =
{
    .canbus_init    = canbus_config,
    .canbus_write   = canbus_write,

    .id = 1
};


void canbus_module_init(void)
{
    canbuschip_add(&canbus1);
    canbuschip_add(&canbus2);
}
