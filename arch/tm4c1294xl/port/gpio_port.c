#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "drivers/gpio/gpio_generic.h"

#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"

static const base[] =
{
    GPIO_PORTA_BASE,    /*  0-7      */
    GPIO_PORTB_BASE,    /*  8-15     */
    GPIO_PORTC_BASE,    /*  16-23    */
    GPIO_PORTD_BASE,    /*  24-31    */
    GPIO_PORTE_BASE,    /*  32-39    */
    GPIO_PORTF_BASE,    /*  40-47    */
    GPIO_PORTG_BASE,    /*  48-55    */
    GPIO_PORTH_BASE,    /*  56-63    */
    GPIO_PORTJ_BASE,    /*  64-71    */
    GPIO_PORTK_BASE,    /*  72-79    */
    GPIO_PORTL_BASE,    /*  80-87    */
    GPIO_PORTM_BASE,    /*  88-95    */
    GPIO_PORTN_BASE,    /*  96-103   */
    GPIO_PORTP_BASE,    /*  104-111  */
    GPIO_PORTQ_BASE     /*  112-119  */
};

static const uint32_t sysctl[] =
{
    SYSCTL_PERIPH_GPIOA,
    SYSCTL_PERIPH_GPIOB,
    SYSCTL_PERIPH_GPIOC,
    SYSCTL_PERIPH_GPIOD,
    SYSCTL_PERIPH_GPIOE,
    SYSCTL_PERIPH_GPIOF,
    SYSCTL_PERIPH_GPIOG,
    SYSCTL_PERIPH_GPIOH,
    SYSCTL_PERIPH_GPIOJ,
    SYSCTL_PERIPH_GPIOK,
    SYSCTL_PERIPH_GPIOL,
    SYSCTL_PERIPH_GPIOM,
    SYSCTL_PERIPH_GPION,
    SYSCTL_PERIPH_GPIOP,
    SYSCTL_PERIPH_GPIOQ
};

static void __gpio_enable_clock(uint8_t index)
{
    if(!SysCtlPeripheralReady(sysctl[index]))
    {
        SysCtlPeripheralEnable(sysctl[index]);
        while(!SysCtlPeripheralReady(sysctl[index]));
    }
}

static void gpio_register_input(uint8_t pin)
{
    uint8_t index = pin / 8;

    __gpio_enable_clock(index);
    GPIOPinTypeGPIOInput(base[index], 1 << (pin % 8));
    GPIOPadConfigSet(base[index], (1 << (pin % 8)), GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);

}

static void gpio_register_output(uint8_t pin)
{
    uint8_t index = pin / 8;

    __gpio_enable_clock(index);
    GPIOPinTypeGPIOOutput(base[index], 1 << (pin % 8));
}

static void gpio_write(uint8_t pin, uint8_t value)
{
    uint8_t index = pin / 8;

    GPIOPinWrite(base[index], 1 << (pin % 8), value == 0 ? 0 : 0xFF);
}

static int8_t gpio_read(uint8_t pin)
{
    uint8_t temp = GPIOPinRead(base[pin / 8], 1 << (pin % 8)) == 0 ? 0 : 0x01;
    return temp;
}

static struct gpio_chip_info gpio_chip_info =
{
    .gpio_register_input    = gpio_register_input,
    .gpio_register_output   = gpio_register_output,
    .gpio_unregister        = NULL,
    .gpio_write             = gpio_write,
    .gpio_request_isr       = NULL,
    .gpio_release_isr       = NULL,
    .gpio_read              = gpio_read,
    .label                  = "tm4c1294/gpio",
    .start_pin              = 0,
    .total_pins             = 20,
    .label                  = "TM4C1294/gpio"
};

static struct gpio_chip_info gpio_chip_info2 =
{
    .gpio_register_input    = gpio_register_input,
    .gpio_register_output   = gpio_register_output,
    .gpio_unregister        = NULL,
    .gpio_write             = gpio_write,
    .gpio_request_isr       = NULL,
    .gpio_release_isr       = NULL,
    .gpio_read              = gpio_read,
    .label                  = "tm4c1294/gpio",
    .start_pin              = 21,
    .total_pins             = 95,
    .label                  = "TM4C1294XL/gpio"
};

void gpio_module_init(void)
{
    gpiochip_add(&gpio_chip_info);
    gpiochip_add(&gpio_chip_info2);
}
