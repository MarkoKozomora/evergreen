#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"

#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "driverlib/prcm.h"
#include "prcm.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"

static uint8_t udma_control_table[512] __attribute__ ((aligned(1024)));

uint8_t udma_init(void)
{
    if(PRCMPeripheralStatusGet(PRCM_UDMA) == false)
    {
        PRCMPeripheralClkEnable(PRCM_UDMA, PRCM_RUN_MODE_CLK);
        PRCMPeripheralReset(PRCM_UDMA);
        while(PRCMPeripheralStatusGet(PRCM_UDMA) == false);
    }

    IntEnable(INT_UDMAERR);
    uDMAEnable();
    uDMAControlBaseSet(udma_control_table);
}

void udma_isr_error_handler(void)
{
    uint32_t status = uDMAErrorStatusGet();

    if(status)
    {
        /* DMA error. This can happed on bus error while trying
         * to perform a transfer.
         * For now, just clear the error flag.
         * TODO: Should notify upper layers about failed DMA transfer.
         */

        uDMAErrorStatusClear();
    }
}
