#include <stdio.h>
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "interrupt.h"
#include "hw_apps_rcm.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
#include <stdbool.h>
#include <stdint.h>
#include "kernel/arch_sys_control.h"
#include "config/evergreen.h"

#include <stdio.h>
#include <stdbool.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"

#include "driverlib/rom.h"
#include "driverlib/rom_map.h"



extern void (* const g_pfnVectors[]);
void early_init(void)
{
	IntVTableBaseSet((unsigned long)&g_pfnVectors[0]); 	// init vector table
	IntMasterEnable(); 									// enable exceptions
	IntEnable(FAULT_SYSTICK); 							// enable systick exception
	PRCMCC3200MCUInit();
}

void system_reset(void)
{
    //SycCtlReset();
}

void system_suspend(void)
{
#if (EVERNGREEN_SLEEP_ENABLED == 1)
    SysCtlSleep();
#endif
}

void system_save_buffer_to_nv_memory(uint8_t address, uint8_t* buffer, uint8_t items)
{

}
