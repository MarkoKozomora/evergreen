#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>

#include "kernel/boot.h"

#include "FreeRTOS.h"
#include "task.h"

#include "drivers/logger/logger.h"
#include "drivers/uart/uartlib.h"
#include "drivers/gpio/gpiolib.h"
#include "drivers/timer/timerlib.h"

#include "apps/eurobot/job.h"
#include "apps/eurobot/robot/canbus_gateway.h"
#include "apps/eurobot/robot/atax.h"
#include "apps/eurobot/robot/odometry.h"

#include "apps/eurobot/robot/macros.h"

#define JOB_ODOMETRY_STOP_WAIT  (1 << 1)
#define JOB_ODOMETRY_RESTART    (1 << 3)
#define JOB_ODOMETRY_SUCCESS    (1 << 4)
#define JOB_ODOMETRY_STUCK      (1 << 5)
#define JOB_ODOMETRY_ERROR      (1 << 6)

#define JOB_ACTUATORS_SUCCESS   (1 << 7)
#define JOB_ACTUATORS_ERROR     (1 << 8)

#define JOB_DETECTIONS_ACTIVE   (1 << 9)

#define JOB_TAG     "JOBS"

struct job_internal
{
    TickType_t      start_time;
    bool            available;

    bool            time_flag_satisfied;
    uint8_t         state;

    bool            odometry_moving;

    TaskHandle_t    odometry_thread;
    TaskHandle_t    actuators_thread;
    TaskHandle_t    detections_thread;
};

extern uint32_t _jobs_start;
extern uint32_t _jobs_end;

static TaskHandle_t job_service_thread;
LIST_HEAD(jobs);


uint8_t job_add_to_jobs_pool(struct job* job)
{
    if(job == NULL)
        return 1;

    /* Create job internal data */
    job->internal = (struct job_internal*)pvPortMalloc(sizeof(struct job_internal));
    if(job->internal == NULL)
        return 1;

    job->internal->available           = (job->total_dependencies == 0) ? true : false;
    job->internal->state               = job->enabled == true ? JOB_ENABLED_WAITING : JOB_DISABLED_USER;
    job->internal->time_flag_satisfied = false;

    for(uint8_t i = 0; i < job->total_dependencies; i++)
        job->dependencies[i].satisfied = false;

    list_add(&(job->list), &jobs);

    return 0;
}

/* Status can be either JOB_DEPENDENCY_UNLOCK_ON_SUCCESS or JOB_DEPENDENCY_UNLOCK_ON_FAILED */
static uint8_t job_unlock_dependency(uint8_t id, uint8_t status)
{
    uint8_t i;
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(job->id == id)
            continue;

        for(i = 0; i < job->total_dependencies; i++)
        {
            if(job->dependencies[i].id == id && job->dependencies[i].condition == status)
            {
                job->dependencies[i].satisfied = true;
                break;
            }
        }
    }

    return 0;
}

static uint8_t job_check_and_unlock(void)
{
    uint8_t i;
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(xTaskGetTickCount() - job->internal->start_time > job->time_dependency)
            job->internal->time_flag_satisfied = true;

        i = 0;
        while(i < job->total_dependencies)
        {
            if(job->dependencies[i].satisfied == false)
                break;
            i++;
        }

        /* If a while loop has been fully completed then all dependencies have been satisfied */
        if(i == job->total_dependencies && job->internal->time_flag_satisfied == true)
            job->internal->available = true;
    }
}

struct job* job_get_next_job(void)
{
    uint8_t i;
    struct job* job;
    uint8_t max_priority = 0;
    uint8_t max_priority_job_id;
    list_for_each_entry(job, &jobs, list)
    {
        /* Only watch enabled jobs who are waiting */
        if(job->internal->state == JOB_ENABLED_WAITING && job->internal->available == true && job->priority >= max_priority)
        {
            max_priority        = job->priority;
            max_priority_job_id = job->id;
        }
    }

    list_for_each_entry(job, &jobs, list)
    {
        if(job->id == max_priority_job_id)
        {
            debug(JOB_TAG, "Selected job with id %d", job->id);
            return job;
        }
    }

    return NULL;
}

/* This function can be used to block and wait for coordinates to be reached.
 * For example, if user actions who are executed in their own thread must wait
 * for position to be reached, this function can be used.
 * This functions returns JOB_ODOMETRY_SUCCESS, JOB_ODOMETRY_ERROR or
 * JOB_ODOMETRY_STUCK.
 * */
uint8_t job_wait_for_destination_stop(void)
{
    uint32_t notified_value;
    while(xTaskNotifyWait(ULONG_MAX, ULONG_MAX, &notified_value, portMAX_DELAY) != pdTRUE)
        vTaskDelay(5);

    return notified_value;
}

/*
 * Simular to job_wait_for_destination_stop. This functions checks
 * threads notifications bits. If any of them has been set, it means that
 * odometry thread has completed its job or an error has occurred.
 * If none of them are set, it means movement is in progress.
 * This functions returns 0 if robot is still moving,
 * JOB_ODOMETRY_SUCCESS if destination has been reached, JOB_ODOMETRY_ERROR
 * if an error occurred or JOB_ODOMETRY_STUCK if robot got in stuck state.
 * */
uint8_t job_get_odometry_state(void)
{
    uint32_t notified_value;
    if(xTaskNotifyWait(ULONG_MAX, ULONG_MAX, &notified_value, 0) != pdTRUE)
        notified_value = 0;

    return notified_value;
}

void job_stop_moving(uint8_t id)
{
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(id == job->id)
            break;
    }

    xTaskNotify(job->internal->odometry_thread, JOB_ODOMETRY_STOP_WAIT, eSetValueWithOverwrite);
}

void job_continue_moving(uint8_t id)
{
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(id == job->id)
            break;
    }

    xTaskNotify(job->internal->odometry_thread, JOB_ODOMETRY_RESTART, eSetValueWithOverwrite);
}


static void odometry_thread(void* params)
{
    struct job*               job = (struct job*)params;
    struct job_odometry_data* data;
    uint32_t                  notified_value;
    uint8_t                   status;
    bool                      moving;

    data = (job->position);
    odometry_move_to_position((data->position), data->direction, data->speed);
    moving = true;

    while(true)
    {
        notified_value = 0;
        /* Check thread specific bits. If any of them has been set,
         * that means some action has to be taken.
         * Threads that can set these bits are: detection thread and actuators thread.
         * */
        if(xTaskNotifyWait(ULONG_MAX, ULONG_MAX, &notified_value, 200) == pdTRUE)
        {
            if(notified_value & JOB_ODOMETRY_STOP_WAIT)
            {
                debug(JOB_TAG, "Received stop command, job remaining active");
                /*
                 * For stor&wait support. This way if a user, by a detection
                 * function which executes in detection thread notifes the system
                 * that movement has to be stopped, but doesn't want job to be
                 * left, can tell to system to stop and wait. When some
                 * user condition has been satisfied, it can re-enable the
                 * movement - case below.
                 * */
                odometry_stop();
                /* Must exist! Otherwise this thread will query odometry state, will get IDLE and
                 * notify the system and other threads that position has been reached.
                 * */
                moving = false;
            }
            else if(notified_value & JOB_ODOMETRY_RESTART)
            {
                debug(JOB_TAG, "Re-starting odometry movement for job %d", job->id);
                odometry_move_to_position((data->position), data->direction, data->speed);
                moving = true;
            }
        }
        else if(moving == true)
        {
            struct position* position = odometry_get_position();
            debug(JOB_TAG, "Quering odometry status for job %d", job->id);
            debug(JOB_TAG, "Position: (%d, %d)", position->x, position->y);
            status = odometry_get_status();
            debug(JOB_TAG, "Status: %c", status);

            /*
             * Can be either stuck, error or idle. Error should never happen.
             * */
            if(status != ODOMETRY_STATUS_MOVING && status != ODOMETRY_STATUS_ROTATING)
                break;
        }
    }

    /*
     * Notify ALL interested threads. Threads that are interested in
     * odometry state are systems jobs service AND ACTUATORS THREAD.
     * By this point, detections thread doesn't exist anymore- see above.
     * */
    if(status == ODOMETRY_STATUS_IDLE)
    {
        debug(JOB_TAG, "Odometry reached position, IDLE reported. Notifying threads...");
        xTaskNotify(job_service_thread, JOB_ODOMETRY_SUCCESS, eSetValueWithOverwrite);
        xTaskNotify(job->internal->actuators_thread, JOB_ODOMETRY_SUCCESS, eSetValueWithOverwrite);
    }
    else if(status == ODOMETRY_STATUS_ERROR)
    {
        error(JOB_TAG, "Odometry entered ERROR state!");
        xTaskNotify(job_service_thread, JOB_ODOMETRY_ERROR, eSetValueWithOverwrite);
        /*
        * xTaskNotify(job->internal->actuators_thread, JOB_ODOMETRY_ERROR, eSetValueWithOverwrite);
        * */
    }
    else if(status == ODOMETRY_STATUS_STUCK)
    {
        error(JOB_TAG, "Odometry thread entered STUCK state!");
        xTaskNotify(job_service_thread, JOB_ODOMETRY_STUCK, eSetValueWithOverwrite);
    }

    /*
     * Wait for job service to delete this thread.
     * */
    while(true)
        vTaskDelay(1000);
}

/*
 * User actions function must return a result of its operation, so the system
 * can determine status of a job.
 * */
static void actuators_thread(void* params)
{
    struct job* job = (struct job*)params;
    uint8_t     status;

    /*
     * 0 means success.
     * */
    if(job->actions != NULL)
    {
        status = job->actions(job->id);
        status = (status == 0) ? JOB_ACTUATORS_SUCCESS : JOB_ACTUATORS_ERROR;
    }
    else
        status = JOB_ACTUATORS_SUCCESS;

    debug(JOB_TAG, "Actuators thread of job %d exiting with status %d", job->id, status);

    /*
     * Notify job service about result of jobs actions.
     * Then wait for job service to move on and delete this thread.
     * */
    vTaskDelay(200);
    xTaskNotify(job_service_thread, (uint32_t)status, eSetValueWithOverwrite);
    while(true)
        vTaskDelay(1000);
}

static void detections_thread(void* params)
{
    struct job* job = (struct job*)params;
    volatile uint8_t status;

    while(true)
    {
        vTaskDelay(50);
        /*
         * If a user detections functions returns non-zero value,
         * that means that job has to be left. This only matters if odometry
         * is still moving. If not, this thread notifications are ignored
         * by job service. If odometry is still moving and detections return a
         * non-zero value, job will be left and put in JOB_DISABLED_FAILED state.
         * */
        status = job->detections(job->id);
        if(status)
        {
            debug(JOB_TAG, "Detection thread activated job service...");

            /*
             * Notify the system that detections are active. This will result
             * in job being moved to JOB_DISABLED_FAILED state.
             * */
            xTaskNotify(job_service_thread, JOB_DETECTIONS_ACTIVE, eSetValueWithOverwrite);
            vTaskDelay(300);
        }
    }
}

static uint8_t job_total_stop(void)
{
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(job->internal->state == JOB_EXECUTING)
        {
            ec_bus_mutex_lock();

            vTaskDelete(job->internal->odometry_thread);
            if(job->actions!= NULL)
                vTaskDelete(job->internal->actuators_thread);
            if(job->detections != NULL)
                vTaskDelete(job->internal->detections_thread);

            ec_bus_mutex_unlock();
            odometry_stop();
            ec_bus_mutex_lock();
        }
    }
}

static uint8_t job_execute(struct job* job)
{
    if(job == NULL)
    {
        error(JOB_TAG, "No job selected for execution...");
        return 1;
    }
    uint32_t notified_value;
    bool     destination_reached = false;
    bool     actuators_completed = false;

    /*
     * Workaround to clear job service thread notification bits.
     * */
    xTaskNotifyWait(ULONG_MAX, ULONG_MAX, NULL, 5);

    ec_bus_mutex_lock();

    job->internal->state = JOB_EXECUTING;
    xTaskCreate(odometry_thread, "", 200, (void*)job, tskIDLE_PRIORITY, &(job->internal->odometry_thread));
    xTaskCreate(actuators_thread, "", 200, (void*)job, tskIDLE_PRIORITY, &(job->internal->actuators_thread));
    if(job->detections != NULL)
        xTaskCreate(detections_thread, "", 200, (void*)job, tskIDLE_PRIORITY, &(job->internal->detections_thread));

    ec_bus_mutex_unlock();
    while(true)
    {
        notified_value = 0;
        while(xTaskNotifyWait(0, ULONG_MAX, &notified_value, portMAX_DELAY) != pdTRUE)
            vTaskDelay(5);

        /*
         * Odometry error has occurred.
         * Move job to JOB_DISABLED_FAILED state.
         * */
        if(notified_value & JOB_ODOMETRY_ERROR ||
           notified_value & JOB_ODOMETRY_STUCK)
        {
            ec_bus_mutex_lock();
            vTaskDelete(job->internal->odometry_thread);
            if(job->actions != NULL)
                vTaskDelete(job->internal->actuators_thread);
            if(job->detections != NULL)
                vTaskDelete(job->internal->detections_thread);
            ec_bus_mutex_unlock();

            job->internal->state = JOB_DISABLED_FAILED;
            job_unlock_dependency(job->id, JOB_DISABLED_FAILED);

            return;
        }

        if(notified_value & JOB_ACTUATORS_ERROR ||
           ((notified_value & JOB_DETECTIONS_ACTIVE) && destination_reached == false))
        {
            ec_bus_mutex_lock();
            vTaskDelete(job->internal->odometry_thread);
            if(job->actions != NULL)
                vTaskDelete(job->internal->actuators_thread);
            if(job->detections != NULL)
                vTaskDelete(job->internal->detections_thread);
            ec_bus_mutex_unlock();

            if(destination_reached == false)
                odometry_stop();

            job->internal->state = JOB_DISABLED_FAILED;
            job_unlock_dependency(job->id, JOB_DISABLED_FAILED);

            return;
        }

        if(notified_value & JOB_ODOMETRY_SUCCESS)
            destination_reached = true;

        if(notified_value & JOB_ACTUATORS_SUCCESS)
            actuators_completed = true;

        if(actuators_completed && destination_reached)
        {
            ec_bus_mutex_lock();
            vTaskDelete(job->internal->odometry_thread);
            vTaskDelete(job->internal->actuators_thread);
            if(job->detections != NULL)
                vTaskDelete(job->internal->detections_thread);
            ec_bus_mutex_unlock();
            job->internal->state = JOB_COMPLETED_SUCCESS;
            job_unlock_dependency(job->id, JOB_COMPLETED_SUCCESS);

            return;
        }
    }
}

uint8_t job_re_enable(uint8_t id)
{
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(job->id == id)
        {
            job->internal->available = false;
            job->internal->state     = JOB_ENABLED_WAITING;

            return 0;
        }
    }

    return 1;
}

uint8_t job_disable(uint8_t id)
{
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        if(job->id == id)
        {
            job->internal->available = false;
            job->internal->state     = JOB_DISABLED_USER;

            return 0;
        }
    }

    return 1;
}



static void jobs_wait_for_start(void)
{
    vTaskDelay(1000);
    /* TODO: Should wait for GPIO(jumper) */

    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        job->internal->start_time = xTaskGetTickCount();
    }
}

static void jobs_update_start_time(void)
{
    struct job* job;
    list_for_each_entry(job, &jobs, list)
    {
        job->internal->start_time = xTaskGetTickCount();
    }
}

struct position start_position =
{
    .x      = ROBOT_START_POSITION_X,
    .y      = ROBOT_START_POSITION_Y,
    .angle  = ROBOT_START_POSITION_ANGLE
};

static void jobs_add(void);
static void __init job_service(void* params)
{
    vTaskDelay(500);
//    gpio_register_pin(ROBOT_START_JUMPER_PIN, GPIO_DIR_INPUT, false);
    ec_bus_init();
    odometry_init();
    job_service_thread = xTaskGetCurrentTaskHandle();
    vTaskDelay(2000);
//    odometry_set_position(&start_position);

    struct job* job;
    jobs_add();
//    while(gpio_read_pin(ROBOT_START_JUMPER_PIN) )
 //       vTaskDelay(300);

    jobs_update_start_time();
    vTaskDelay(5);

    timer_start_oneshot(1, 89900, job_total_stop);

    while(true)
    {
        job_check_and_unlock();
        job = job_get_next_job();
        job_execute(job);
        vTaskDelay(10);
    }
}

static pjob* jobs_start  = &_jobs_start;
static pjob* jobs_end    = &_jobs_end;

static void jobs_add(void)
{
    pjob* job = jobs_start;
    uint8_t i;
    uint8_t jobs_tot_stop = jobs_end - jobs_start;

    for(i = 0; i < jobs_tot_stop ; i++, job++)
    {
        job_add_to_jobs_pool(*job);
    }
}

ADD_TO_THREAD_POOL(job_service) = job_service;
