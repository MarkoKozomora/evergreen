#ifndef _EUROBOT_JOB_H_INCLUDED
#define _EUROBOT_JOB_H_INCLUDED

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "apps/eurobot/robot/odometry.h"
#include "evergreen/list_generic.h"

#define JOB_COMPLETED_SUCCESS               0
#define JOB_ENABLED_WAITING                 4
#define JOB_DISABLED_FAILED                 2
#define JOB_DISABLED_USER                   3
#define JOB_EXECUTING                       5

#define JOB_DEPENDENCY_UNLOCK_ON_SUCCESS    0
#define JOB_DEPENDENCY_UNLOCK_ON_FAILED     2

typedef struct job* pjob;

#define ADD_TO_JOBS_POOL(x) static pjob __jobs_##x## __used __attribute__((section(".jobs0")))

struct dependeny_internal;
struct job_dependency
{
    uint8_t id;
    uint8_t condition;
    bool    satisfied;
};

struct job_odometry_data
{
    struct position* position;
    uint8_t         speed;
    int8_t          direction;
};

struct job_internal;
struct job
{
    struct job_internal*        internal;
    struct job_odometry_data*   position;
    uint8_t                     (*actions)(uint8_t job_id);
    uint8_t                     (*detections)(uint8_t job_id);
    struct job_dependency*      dependencies;
    uint8_t                     total_dependencies;
    uint32_t                    time_dependency;
    uint8_t                     priority;
    bool                        enabled;

    uint8_t                     id;

    struct list_head            list;
};

uint8_t job_add_to_jobs_pool(struct job* job);

/*
 * Stops the movement of a robot, but keeps the job active and in
 * execution. This function can be used to implement stop & wait feature.
 * */
void    job_stop_moving(uint8_t job_id);
void    job_continue_moving(uint8_t job_id);
uint8_t job_wait_for_destination_stop(void);
uint8_t job_get_odometry_state(void);
uint8_t job_re_enable(uint8_t job_id);
uint8_t job_disable(uint8_t id);
#endif
