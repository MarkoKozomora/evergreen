#include <stdint.h>
#include <stdbool.h>

#include "drivers/logger/logger.h"

#include "apps/eurobot/job.h"
#include "apps/eurobot/robot/odometry.h"
#include "apps/eurobot/robot/atax.h"

#include "apps/eurobot/robot/macros.h"



struct position tower_load_off_position =
{
    .x = 1000,
    .y = 1070
};

struct position pucks_pick_up_position =
{
    .x = 888,
    .y = 1203
};

struct position pucks_pick_up_all_position =
{
    .x = 770,
    .y = 1325
};

struct position pucks_home_position_pucks =
{
    .x = 360, // 400
    .y = 1370 //1345
};

struct position pucks_load_off_home_position =
{
    .x = 250,
    .y = 995
};

struct job_odometry_data job_odometry_tower_data =
{
    .position  = &tower_load_off_position,
    .direction = ODOMETRY_DIRECTION_BACKWARDS,
    .speed     = 120
};

struct job_odometry_data job_odometry_pucks_data =
{
    .position  = &pucks_pick_up_position,
    .direction = ODOMETRY_DIRECTION_FORWARD,
    .speed     = 80
};

struct job_odometry_data job_odometry_home_position_pucks_data =
{
    .position  = &pucks_home_position_pucks,
    .direction = ODOMETRY_DIRECTION_FORWARD,
    .speed     = 100
};

struct job_odometry_data job_pucks_load_off_home_odometry_data =
{
    .position  = &pucks_load_off_home_position,
    .direction = ODOMETRY_DIRECTION_FORWARD,
    .speed     = 60
};

struct job_dependency job_pucks_pick_up_dependency =
{
    .id        = 1,
    .condition = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

struct job_dependency job_pucks_home_position_dependency =
{
    .id        = 2,
    .condition = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

struct job_dependency job_pucks_load_off_home_dependency =
{
    .id        = 3,
    .condition = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

static uint8_t actions_camera(uint8_t job_id)
{
    job_wait_for_destination_stop();
    odometry_rotate(-70, 80);
    vTaskDelay(200);
    atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_OPEN, 180, 1);
    atax_servo_set_angle(DOOR_RIGHT_SERVO_ID, DOOR_RIGHT_POSITION_OPEN, 180, 1);
    vTaskDelay(1000);

    /*pucks_pick_up_position.x = 770;
    pucks_pick_up_position.y = 1325;
    */
    /*
     * Here it should take a picture and wait for GPIO.
     * Set position of a next job here.
     * */

    return 0;
}

static uint8_t actions_pucks_pick_up_home(uint8_t job_id)
{
    job_wait_for_destination_stop();
    vTaskDelay(100);
    atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_CLOSED + 60, 180, 1);
    atax_servo_set_angle(DOOR_RIGHT_SERVO_ID, DOOR_RIGHT_POSITION_CLOSED, 180, 1);
    return 0;
}

static uint8_t actions_pucks_load_off_home(uint8_t job_id)
{
    job_wait_for_destination_stop();
    vTaskDelay(100);
    atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_OPEN, 180, 1);
    atax_servo_set_angle(DOOR_RIGHT_SERVO_ID, DOOR_RIGHT_POSITION_OPEN, 180, 1);
    vTaskDelay(100);
    odometry_move_straight(400, ODOMETRY_DIRECTION_BACKWARDS, 40);
    vTaskDelay(3100);

    return 0;
}

struct job tower_job =
{
    .position           = &job_odometry_tower_data,
    .actions            = actions_camera,
    .detections         = NULL,
    .id                 = 1,
    .enabled            = true,
    .priority           = 10,
    .time_dependency    = 1200,
    .total_dependencies = 0,
    .dependencies       = NULL
};

struct job job_pucks_pick_up =
{
    .position           = &job_odometry_pucks_data,
    .actions            = NULL,
    .detections         = NULL,
    .id                 = 2,
    .enabled            = true,
    .priority           = 10,
    .time_dependency    = 0,
    .total_dependencies = 1,
    .dependencies       = &job_pucks_pick_up_dependency
};

struct job job_pick_up_home_pucks =
{
    .position           = &job_odometry_home_position_pucks_data,
    .actions            = actions_pucks_pick_up_home,
    .id                 = 3,
    .enabled            = true,
    .priority           = 10,
    .time_dependency    = 0,
    .total_dependencies = 1,
    .dependencies       = &job_pucks_home_position_dependency
};

struct job job_load_off_pucks_home =
{
    .position           = &job_pucks_load_off_home_odometry_data,
    .actions            = actions_pucks_load_off_home,
    .id                 = 4,
    .enabled            = true,
    .priority           = 20,
    .time_dependency    = 0,
    .total_dependencies = 1,
    .dependencies       = &job_pucks_load_off_home_dependency
};

void add_tower_job(void)
{
    job_add_to_jobs_pool(&tower_job);
//    job_add_to_jobs_pool(&job_pucks_pick_up);
    //job_add_to_jobs_pool(&job_pick_up_home_pucks);
    //job_add_to_jobs_pool(&job_load_off_pucks_home);
}

