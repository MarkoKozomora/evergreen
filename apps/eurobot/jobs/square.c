#include <stdint.h>
#include <stdbool.h>

#include "drivers/logger/logger.h"

#include "apps/eurobot/job.h"
#include "apps/eurobot/robot/odometry.h"
#include "apps/eurobot/robot/atax.h"

#include "apps/eurobot/robot/macros.h"

struct position coords_four =
{
    .x = 0,
    .y = 0
};

struct position coords_one =
{
    .x = 1920,
    .y = 0
};

struct position coords_two =
{
    .x = 1920,
    .y = 1200
};

struct position coords_three =
{
    .x = 0,
    .y = 1200
};

struct job_odometry_data data_one =
{
    .position  = &coords_one,
    .speed     = 70,
    .direction = ODOMETRY_DIRECTION_FORWARD
};

struct job_odometry_data data_two =
{
    .position  = &coords_two,
    .speed     = 100,
    .direction = ODOMETRY_DIRECTION_FORWARD
};

struct job_odometry_data data_three =
{
    .position  = &coords_three,
    .speed     = 60,
    .direction = ODOMETRY_DIRECTION_FORWARD
};

struct job_odometry_data data_four =
{
    .position  = &coords_four,
    .speed     = 100,
    .direction = ODOMETRY_DIRECTION_FORWARD
};

uint8_t actions(uint8_t job_id)
{
    if(job_id == 4)
    {
        job_wait_for_destination_stop();
        vTaskDelay(500);
        odometry_set_angle(0, 100);
    }

    else if(job_id == 1)
    {
        job_wait_for_destination_stop();
        atax_servo_set_angle(FISH_CATCHER_SERVO_ID, FISH_CATCHER_POSITION_DOWN, 150, 1);
        vTaskDelay(200);
    }
    else if(job_id == 2)
    {
        job_wait_for_destination_stop();
        atax_servo_set_angle(FISH_CATCHER_SERVO_ID, FISH_CATCHER_POSITION_UP, 150, 1);
        vTaskDelay(200);
    }
    else if(job_id == 3)
    {
        vTaskDelay(1000);
        atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_OPEN, 150, 1);
        atax_servo_set_angle(FISH_CATCHER_SERVO_ID, 190, 150, 1);

        vTaskDelay(2000);
        atax_servo_set_angle(FISH_CATCHER_SERVO_ID, FISH_CATCHER_POSITION_UP, 150, 1);
        //atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_OPEN, 150, 1);
        atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_CLOSED, 150, 0);
    }
    return 0;
}

struct job  job_position_one =
{
    .position    = &data_one,
    .actions     = actions,
    .detections  = NULL,
    .id          = 1,
    .enabled     = true,
    .priority    = 1,
    .time_dependency = 67000,
    .total_dependencies = 0,
    .dependencies = NULL
};



struct job_dependency job_two_first =
{
    .id = 1,
    .condition = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

struct job  job_position_two =
{
    .position    = &data_two,
    .actions     = actions,
    .detections  = NULL,
    .id          = 2,
    .enabled     = true,
    .priority    = 4,
    .time_dependency = 0,
    .total_dependencies = 1,
    .dependencies = &job_two_first
};


struct job_dependency job_three_dep_first =
{
    .id = 2,
    .condition = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

struct job job_position_three =
{
    .position    = &data_three,
    .actions     = actions,
    .detections  = NULL,
    .id          = 3,
    .enabled     = true,
    .priority    = 4,
    .time_dependency = 0,
    .total_dependencies = 1,
    .dependencies = &job_three_dep_first
};



struct job_dependency job_four_dep_first =
{
    .id = 3,
    .condition = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

  struct job job_position_four =
{
    .position    = &data_four,
    .actions     = actions,
    .detections  = NULL,
    .id          = 4,
    .enabled     = true,
    .priority    = 7,
    .time_dependency = 0,
    .total_dependencies = 1,
    .dependencies = &job_four_dep_first
};

/*
ADD_TO_JOBS_POOL(one) = &job_position_one;
ADD_TO_JOBS_POOL(two) = &job_position_two;
ADD_TO_JOBS_POOL(three) = &job_position_three;
ADD_TO_JOBS_POOL(four) = &job_position_four;
*/
