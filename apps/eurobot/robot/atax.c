#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "drivers/uart/uartlib.h"
#include "drivers/logger/logger.h"

#include "apps/eurobot/robot/canbus_gateway.h"
#include "apps/eurobot/robot/atax.h"

#define LOG_TAG     "ATAX"

void atax_servo_set_angle(uint8_t servo_id, int16_t angle, uint16_t speed, uint8_t dir)
{
    uint8_t data[8];
    data[0] = 0;
    data[1] = servo_id;
    data[2] = angle >> 8;
    data[3] = angle & 0xFF;
    data[4] = speed >> 8;
    data[5] = speed & 0xFF;
    data[6] = dir;

    struct ec_bus_packet packet =
    {
        .id                 = AX12_CANBUS_MSG_IDE,
        .items              = 8,
        .payload            = data
    };

    debug(LOG_TAG, "Sending data to canbus gateway layer...");
    ec_bus_send_message(&packet);
    debug(LOG_TAG, "Data sent");
}

void atax_gpio_init(uint8_t port, uint8_t pin, uint8_t dir)
{
    uint8_t data[8];
    data[0] = 0;
    data[1] = port;
    data[2] = pin;
    data[3] = dir;

    struct ec_bus_packet packet =
    {
        .id                 = ATAX_GPIO_CANBUS_MSG_IDE,
        .payload            = data,
        .items              = 8
    };

    debug(LOG_TAG, "Sending data to canbus gateway layer...");
    ec_bus_send_message(&packet);
    debug(LOG_TAG, "Data sent");
}

void atax_gpio_set_value(uint8_t port, uint8_t pin, uint8_t value)
{
    uint8_t data[8];
    data[0] = 1;
    data[1] = port;
    data[2] = pin;
    data[3] = value;

    struct ec_bus_packet packet =
    {
        .id                 = ATAX_GPIO_CANBUS_MSG_IDE,
        .payload            = data,
        .items              = 8
    };

    debug(LOG_TAG, "Sending data to canbus gateway layer...");
    ec_bus_send_message(&packet);
    debug(LOG_TAG, "Data sent");
}
