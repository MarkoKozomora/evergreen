#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>

#include "apps/eurobot/robot/canbus_gateway.h"
#include "drivers/uart/uartlib.h"
#include "drivers/gpio/gpiolib.h"
#include "drivers/uart/uart_generic.h"
#include "drivers/logger/logger.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#define TAG     "GATEWAY"

#define EC_BUS_DATA_SENT                (1 << 1)
#define EC_BUS_ACK_RECEIVED             (1 << 2)
#define EC_BUS_DATA_RECEVED             (1 << 5)
#define EC_BUS_ERROR_RECEIVED           (1 << 7)

#ifdef PART_CC3200
#define EC_BUS_TX_LED                   9
#define EC_BUS_RX_LED                   10
#endif

struct ec_listener
{
    uint8_t         canbus_id;
    void             (*listener)(uint16_t id, uint8_t* data);

    struct list_head list;
};

static SemaphoreHandle_t gateway_mutex;
static TaskHandle_t      calling_thread_id;
LIST_HEAD(bus_listeners);

void canbus_gateway_listener(uint8_t num, uint8_t event, uint8_t* data, uint8_t items)
{
    if(event == UART_TX_EVENT)
    {
        debug(TAG, "EC-bus packet sent");
        xTaskNotify(calling_thread_id, EC_BUS_DATA_SENT, eSetValueWithOverwrite);
    }
    else if(event == UART_RX_EVENT)
    {
        gpio_write_pin(EC_BUS_RX_LED, true);
        debug(TAG, "First byte: %c", data[0]);
        struct ec_listener* listener;
        list_for_each_entry(listener, &bus_listeners, list)
        {
            if(listener->canbus_id == data[0])
                listener->listener(data[0], data + 1);
        }

        vPortFree(data);
        gpio_write_pin(EC_BUS_RX_LED, false);
    }
}

void ec_bus_init(void)
{
    debug(TAG, "Setting EC-bus UART...");
    struct uart_config config =
    {
        .baud_rate   = EC_BUS_UART_BAUDRATE,
        .parity_mode = UART_PARITY_NONE,
        .stop_bits   = UART_STOP_BITS_ONE,
        .tx_pin      = EC_BUS_UART_TX_PIN,
        .rx_pin      = EC_BUS_UART_RX_PIN
    };
    uart_init(EC_BUS_UART_PORT, &config);
    uart_register_listener(EC_BUS_UART_PORT, canbus_gateway_listener);

    debug(TAG, "Creating EC-bus mutex...");
    gateway_mutex = xSemaphoreCreateMutex();
    vTaskDelay(100);

#ifdef PART_CC3200
    gpio_register_pin(EC_BUS_TX_LED, GPIO_DIR_OUTPUT, false);
    gpio_register_pin(EC_BUS_RX_LED, GPIO_DIR_OUTPUT, false);
#endif
}

uint8_t  ec_bus_register_listener(uint8_t id, void (*listener)(uint8_t id, uint8_t* data))
{
    if(listener == NULL)
    {
        error(TAG, "Trying to register NULL listener");
        return 1;
    }

    struct ec_listener* ec_listener = (struct ec_listener*)pvPortMalloc(sizeof(struct ec_listener));
    if(ec_listener == NULL)
    {
        error(TAG, "Not enough memory to register EC-bus listener");
        return 1;
    }

    ec_listener->canbus_id       = id;
    ec_listener->listener        = listener;
    list_add(&(ec_listener->list), &bus_listeners);

    return 0;
}

uint8_t ec_bus_send_message(struct ec_bus_packet* packet)
{
    while(xSemaphoreTake(gateway_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    gpio_write_pin(EC_BUS_TX_LED, true);
    uint8_t result = 0;
    uint8_t* buffer = (uint8_t*)pvPortMalloc(10 * sizeof(uint8_t));
    if(buffer == NULL)
    {
        fatal(TAG, "Not enough memory for gateway message...");
        result = 1;
        goto exit;
    }

    buffer[0] = (packet->id) >> 8;
    buffer[1] = (packet->id) & 0xFF;

    uint8_t i;
    for(i = 0; i < packet->items; i++)
        buffer[i + 2] = packet->payload[i];

    calling_thread_id = xTaskGetCurrentTaskHandle();
    do
    {
        while(uart_write_buffer(EC_BUS_UART_PORT, buffer, 10))
            vTaskDelay(2);
    }while(xTaskNotifyWait(ULONG_MAX, ULONG_MAX, NULL, portMAX_DELAY) != pdTRUE);
    vPortFree(buffer);

exit:
    gpio_write_pin(EC_BUS_TX_LED, false);
    xSemaphoreGive(gateway_mutex);
    return result;
}

void ec_bus_mutex_lock(void)
{
    while(xSemaphoreTake(gateway_mutex, portMAX_DELAY) != pdTRUE)
        vTaskDelay(2);
}

void ec_bus_mutex_unlock(void)
{
    xSemaphoreGive(gateway_mutex);
}

