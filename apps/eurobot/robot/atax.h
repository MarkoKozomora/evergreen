#ifndef _ATAX_H_INCLUDED
#define _ATAX_H_INCLUDED

#define AX12_CANBUS_MSG_IDE             0xAB
#define AX12_ROTATION_CLOCKWISE         0x0
#define AX12_ROTATION_COUNTER_CLOCKWISE 0x1

#define ATAX_GPIO_CANBUS_MSG_IDE        0xC0
#define ATAX_GPIO_DIRECTION_INPUT       0X00
#define ATAX_GPIO_DIRECTION_OUTPUT      0X01

void atax_servo_set_angle(uint8_t servo_id, int16_t angle, uint16_t speed, uint8_t direction);
void atax_gpio_set_value(uint8_t port, uint8_t pin, uint8_t value);
void atax_gpio_init(uint8_t port, uint8_t pin, uint8_t direction);

#endif
