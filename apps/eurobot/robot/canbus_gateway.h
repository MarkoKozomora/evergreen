#ifndef _CANBUS_GATEWAY_H_INCLUDED
#define _CANBUS_GATEWAY_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#ifdef PART_CC3200
#define EC_BUS_UART_PORT            1
#define EC_BUS_UART_TX_PIN          58
#define EC_BUS_UART_RX_PIN          59
#endif

#ifdef PART_TM4C1294NCPDT
#define EC_BUS_UART_PORT            5
#define EC_BUS_UART_TX_PIN          23
#define EC_BUS_UART_RX_PIN          22
#endif

#define EC_BUS_UART_BAUDRATE        57600
#define EC_BUS_COMMAND              0x20
#define EC_BUS_ACK                  0x60
#define EC_BUS_ERROR                0x70
#define EC_BUS_REMOTE_FRAME         0x85

struct ec_bus_packet
{
    uint8_t  id;
    uint8_t  items;
    uint8_t* payload;
};

uint8_t  ec_bus_send_message(struct ec_bus_packet* packet);
void     ec_bus_init(void);
uint8_t  ec_bus_register_listener(uint8_t id, void (*listener)(uint8_t id, uint8_t* data));
void     ec_bus_mutex_lock(void);
void     ec_bus_mutex_unlock(void);

#endif
