#ifndef _ODOMETRY_H_INCLUDED_
#define _ODOMETRY_H_INCLUDED_

#define ODOMETRY_CANBUS_MSG_IDE             0xC3

#define ODOMETRY_DIRECTION_FORWARD          1
#define ODOMETRY_DIRECTION_BACKWARDS        -1

#define ODOMETRY_STATUS_IDLE                'I'
#define ODOMETRY_STATUS_MOVING              'M'
#define ODOMETRY_STATUS_ROTATING            'R'
#define ODOMETRY_STATUS_STUCK               'S'
#define ODOMETRY_STATUS_ERROR               'E'

#define ODOMETRY_ACTION_SET_POSITION        'I'
#define ODOMETRY_ACTION_MOVE_TO_POSITION    'G'
#define ODOMETRY_ACTION_MOVE_STRAIGHT       'D'
#define ODOMETRY_ACTION_ROTATE              'T'
#define ODOMETRY_ACTION_SET_ANGLE           'A'
#define ODOMETRY_ACTION_CIRCLE_MOVEMENT     'Q'
#define ODOMETRY_ACTION_SET_SPEED           'V'
#define ODOMETRY_ACTION_GET_STATUS          'P'
#define ODOMETRY_ACTION_STOP_HARD           'S'
#define ODOMETRY_ACTION_STOP_SOFT           's'

struct position
{
    int16_t x;
    int16_t y;
    int16_t angle;
};

void                          odometry_set_position(struct position* position);
void                          odometry_stop(void);
void                          odometry_wait_additional(uint16_t delay);
uint8_t                       odometry_set_speed(uint8_t speed);
const struct position* const  odometry_get_position(void);
uint8_t                       odometry_get_status(void);
uint8_t                       odometry_set_angle(int16_t angle, uint8_t speed);
uint8_t                       odometry_rotate(int16_t angle, uint8_t speed);
uint8_t                       odometry_move_straight(uint16_t distance, int8_t direction, uint8_t speed);
uint8_t                       odometry_move_to_position(struct position* position, int8_t direction, uint8_t speed);
uint8_t                       odometry_circle_movement(struct position* center, int16_t arc, int8_t direction, uint8_t speed);

#endif
