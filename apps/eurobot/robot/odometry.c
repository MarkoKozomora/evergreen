#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "kernel/boot.h"
#include "drivers/uart/uartlib.h"
#include "drivers/logger/logger.h"

#include "apps/eurobot/robot/odometry.h"
#include "apps/eurobot/robot/canbus_gateway.h"

#define ODOMETRY_LOG_TAG  "ODOMETRY"

#define ODOMETRY_ACTION_SUCCESS     (1 << 1)
#define ODOMETRY_ACTION_ERROR       (1 << 2)

static struct position* current_position;
static uint8_t          current_speed;
static QueueHandle_t    odometry_queue;

static void odometry_listener(uint8_t id, uint8_t* data)
{
    current_position->x     = (data[1] << 8) | data[2];
    current_position->y     = (data[3] << 8) | data[4];
    current_position->angle = (data[5] << 8) | data[6];
    while(xQueueSend(odometry_queue, &data[0], portMAX_DELAY) != pdTRUE)
        vTaskDelay(2);
//    debug(ODOMETRY_LOG_TAG, "Reported: (%d, %d)", current_position->x, current_position->y);
}

void odometry_init(void)
{
    current_position = (struct position*)pvPortMalloc(sizeof(struct position));
    odometry_queue   = xQueueCreate(5, sizeof(uint8_t));
    if(current_position == NULL || odometry_queue == NULL)
    {
        fatal(ODOMETRY_LOG_TAG, "Can't allocate memory for odometry driver");
        system_reset();
    }

    ec_bus_register_listener(0x6A, odometry_listener);
}

uint8_t odometry_set_speed(uint8_t speed)
{
    if(current_speed == speed)
        return 0;

    uint8_t data[8];

    data[0] = ODOMETRY_ACTION_SET_SPEED;
    data[1] = speed;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Setting odometry speed to %d", speed);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Speed data sent");

    current_speed = speed;
    return 0;
}

uint8_t odometry_get_status(void)
{
    uint8_t  data[8];
    uint8_t  status;

    data[0] = ODOMETRY_ACTION_GET_STATUS;
    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    do
    {
        debug(ODOMETRY_LOG_TAG, "Querying odometry status...");
        ec_bus_send_message(&packet);

        debug(ODOMETRY_LOG_TAG, "Query sent");
    }while(xQueueReceive(odometry_queue, (void*)&status, 300) != pdTRUE);

    debug(ODOMETRY_LOG_TAG, "Odometry status received: %d", status);
    return status;
}

const struct position* const odometry_get_position(void)
{
    return current_position;
}

uint8_t odometry_move_to_position(struct position* position, int8_t direction, uint8_t speed)
{
    odometry_set_speed(speed);
    uint8_t data[8];

    data[0] = ODOMETRY_ACTION_MOVE_TO_POSITION;

    data[1] = (position->x) >> 8;
    data[2] = (position->x) & 0xFF;

    data[3] = (position->y) >> 8;
    data[4] = (position->y) & 0xFF;

    data[5] = 0;
    data[6] = direction;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Moving robot to (%d, %d)", position->x, position->y);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Odometry position data sent");

    return 0;
}

uint8_t odometry_move_straight(uint16_t distance, int8_t direction, uint8_t speed)
{
    odometry_set_speed(speed);
    uint8_t data[8];
    int16_t dist = distance * direction;

    data[0] = ODOMETRY_ACTION_MOVE_STRAIGHT;

    data[1] = dist >> 8;
    data[2] = dist & 0xFF;
    data[3] = 0;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Moving robot for %d mm", dist);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Odometry data sent");

    return 0;
}

uint8_t odometry_set_angle(int16_t angle,  uint8_t speed)
{
    odometry_set_speed(speed);
    uint8_t data[8];

    data[0] = ODOMETRY_ACTION_SET_ANGLE;

    data[1] = angle >> 8;
    data[2] = angle & 0xFF;
    data[3] = 0;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Setting robot angle to: %d", angle);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Angle data sent");

    return 0;
}

uint8_t odometry_rotate(int16_t angle,  uint8_t speed)
{
    odometry_set_speed(speed);
    uint8_t data[8];

    data[0] = ODOMETRY_ACTION_ROTATE;
    data[1] = angle >> 8;
    data[2] = angle & 0xFF;
    data[3] = 0;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Rotating robot for %d degrees", angle);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Angle data sent");

    return 0;
}

uint8_t odometry_circle_movement(struct position* center, int16_t arc_distance, int8_t direction, uint8_t speed)
{
    odometry_set_speed(speed);
    uint8_t data[8];

    data[0] = ODOMETRY_ACTION_CIRCLE_MOVEMENT;
    data[1] = (center->x) >> 8;
    data[2] = (center->x) & 0xFF;
    data[3] = (center->y) >> 8;
    data[4] = (center->y) & 0xFF;
    data[5] = arc_distance >> 8;
    data[6] = arc_distance & 0xFF;
    data[7] = direction;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Entering circle movement around (%d, %d) for %d degrees...", center->x,
                                                                                          center->y,
                                                                                          arc_distance);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Circle movement data sent");

    return 0;
}

void odometry_set_position(struct position* position)
{
    uint8_t data[8];

    data[0] = ODOMETRY_ACTION_SET_POSITION;
    data[1] = (position->x) >> 8;
    data[2] = (position->x) & 0xFF;
    data[3] = (position->y) >> 8;
    data[4] = (position->y) & 0xFF;
    data[5] = (position->angle) >> 8;
    data[6] = (position->angle) & 0xFF;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Setting robot position to (%d, %d, %d)", position->x, position->y, position->angle);
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Odometry position set.");

    current_position->x     = position->x;
    current_position->y     = position->y;
    current_position->angle = position->angle;
}

void odometry_stop(void)
{
    uint8_t data[8];
    data[0] = ODOMETRY_ACTION_STOP_HARD;

    struct ec_bus_packet packet =
    {
        .id      = ODOMETRY_CANBUS_MSG_IDE,
        .items   = 8,
        .payload = data
    };

    debug(ODOMETRY_LOG_TAG, "Stopping robot immediatelly");
    ec_bus_send_message(&packet);
    debug(ODOMETRY_LOG_TAG, "Odometry stop request sent.");
}

void odometry_wait_additional(uint16_t delay)
{
    vTaskDelay(100);
    while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
        vTaskDelay(150);

    if(delay > 0)
        vTaskDelay(delay);
}
