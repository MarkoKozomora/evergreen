#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "kernel/boot.h"
#include "FreeRTOS.h"
#include "task.h"

#include "apps/eurobot/robot/atax.h"
#include "apps/eurobot/robot/canbus_gateway.h"

#include "drivers/logger/logger.h"
#include "drivers/gpio/gpiolib.h"
#include "drivers/timer/timerlib.h"

#define LOG_TAG     "TEST"

void timer_callback(uint8_t id, uint8_t skips)
{
    debug("TIMER", "Heap: %d", xPortGetFreeHeapSize());
}

static void __init ping_thread(void* params)
{
    vTaskDelay(1000);
//    timer_start_repeating(1, 1000, timer_callback);

    while(1)
    {

        debug("PING", "Heap: %d", xPortGetFreeHeapSize());
        vTaskDelay(1000);

    }
}

ADD_TO_THREAD_POOL(ping_thread) = ping_thread;
