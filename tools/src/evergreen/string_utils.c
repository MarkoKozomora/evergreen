#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "evergreen/string_utils.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

const char* const string_format(const char* const text, ...)
{
    va_list list;
    uint16_t max_size = BUFFER_START_SIZE;
    int8_t status;
    char* buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));

    while(true)
    {
        if(buffer == NULL)
            return NULL;

        va_start(list, text);
        status = tfp_vsnprintf(buffer, max_size, text, list);
        va_end(list);

        if(status > -1 && status < max_size)
            return buffer;

        max_size += 8;
        vPortFree(buffer);
        buffer = (char*)pvPortMalloc(max_size * sizeof(uint8_t));
    }

    return NULL;
}
