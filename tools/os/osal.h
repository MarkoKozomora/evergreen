#ifndef _OSAL_H_INCLUDED
#define _OSAL_H_INCLUDED

#ifdef USE_FREERTOS
#include "FreeRTOS.h"
#include "tasks.h"
#include "semphr.h"
#include "queue.h"

#define os_thread_handle    TaskHandle_t
#define os_tick_type        TickType_t
#define os_queue_handle     QueueHandle_t

#define os_join_thread_pool(func, priority, args, stack_size, handle)   \
    xTaskCreate(func, "", stack_size, args, priority, handle)

#define os_queue_create(total_items, item_size) xQueueCreate(total_items, item_size)
#define os_queue_send_to_back


#endif /* USE_FREERTOS*/
#endif /* _OSAL_H_INCLUDED */
