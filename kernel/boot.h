#ifndef _BOOT_H_INCLUDED
#define _BOOT_H_INCLUDED

#include "evergreen/list_generic.h"

typedef void (*initcall_t)(void*);


#define ADD_TO_THREAD_POOL(x) static initcall_t __initcall_##x## __used __attribute__((section(".init0")))
#define __init __attribute__((section(".init1")))

#define ADD_USER_THREAD(fn) (ADD_TO_THREAD_POOL(fn) = fn)

#endif
