#include "FreeRTOS.h"
#include "task.h"

#include "boot.h"
#include "kernel/arch_sys_control.h"
#include "config/evergreen.h"
#include "evergreen/list_generic.h"

#include "drivers/hooks/hooks.h"
#include "drivers/gpio/gpiolib.h"
#include "drivers/uart/uartlib.h"
#include "drivers/canbus/canbuslib.h"
#include "drivers/timer/timerlib.h"


extern initcall_t __initcall0_start[];
extern uint32_t _init_start;
extern uint32_t _init_end;
extern uint32_t _init_size;

/*
static initcall_t*  initcalls[] __attribute__ ((__section__(".init0"))) =
{
    _init_start
};
*/
static initcall_t* start __section(".init0") = &_init_start;
static initcall_t* stop __section(".init1") = &_init_end;
//static uint32_t size __section(".init0") = &_init_size;
static void do_initcalls(void)
{
    initcall_t* fn = start;// = (initcall_t*)&_init_start;

    uint16_t i = 0;
    fn = start;
    while(i < (stop - start - 1))
    {
    //for(fn = start; fn < stop; fn += 1)
        xTaskCreate(*fn, "", 500, NULL, tskIDLE_PRIORITY, NULL);
        fn++;
        i++;
    }
}

static void freertos_kernel_boot(void)
{
#if (EVERGREEN_LOGGER_ENABLED == 1)
    logger_init();
#endif

    hooks_init();
    gpio_service_init();
#if (EVERGREEN_UART_ENABLED == 1)
    uart_service_init();
#endif
#if (EVERGREEN_CANBUS_ENABLED == 1)
    canbus_service_init();
#endif
#if (EVERGREEN_TIMERS_ENABLED == 1)
    timer_service_init();
#endif

    do_initcalls();

    vTaskStartScheduler();
}

int main(void)
{
    early_init();
    freertos_kernel_boot();
}
