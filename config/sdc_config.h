#ifndef  _SDC_CONFIG_H_INCLUDED
#define  _SDC_CONFIG_H_INCLUDED

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"

#define SDC_SSI_BASE            SSI1_BASE
#define SDC_SSI_SYSCTL_PERIPH   SYSCTL_PERIPH_SSI1

/* GPIO for SSI pins */
/* CLK pin */
#define SDC_SSI_CLK_GPIO_PORT_BASE      GPIO_PORTB_BASE
#define SDC_SSI_CLK                     GPIO_PIN_5
/* TX pin */
#define SDC_SSI_TX_GPIO_PORT_BASE       GPIO_PORTE_BASE
#define SDC_SSI_TX                      GPIO_PIN_4
/* RX pin */
#define SDC_SSI_RX_GPIO_PORT_BASE       GPIO_PORTE_BASE
#define SDC_SSI_RX                      GPIO_PIN_5
/* CS pin */
#define SDC_SSI_FSS_GPIO_PORT_BASE      GPIO_PORTB_BASE
#define SDC_SSI_FSS                     GPIO_PIN_4

#endif
