# Makfile for Tiva C Connected EK-TM4C1294XL Lauchpad Board FreeRTOS projects
# Author: Sava Jakovljev

TARGET_NAME=evergreen
#PART=TM4C1294NCPDT
PART=CC3200
OUTDIR=out


# Base of project to be built
PROJECT_ROOT=$(shell pwd)

# Base of FreeRTOS
FREERTOS_ROOT=${PROJECT_ROOT}/kernel

DEBUG="true"


# The default rule, which causes the driver library to be built
all: ${OUTDIR}
all: ${OUTDIR}/${TARGET_NAME}.axf
${OUTDIR}:
	@echo "tst"
	@mkdir ${OUTDIR}

VPATH=${PROJECT_ROOT}
IPATH=${PROJECT_ROOT}

ifeq (${PART}, TM4C1294NCPDT)
include ${PROJECT_ROOT}/arch/tm4c1294xl/makedefs
IPATH+=${PROJECT_ROOT}/arch/tm4c1294xl/
IPATH+=${PROJECT_ROOT}/arch/tm4c1294xl/inc
IPATH+=${PROJECT_ROOT}/arch/tm4c1294xl/driverlib
IPATH+=${FREERTOS_ROOT}/portable/GCC/ARM_CM4F

VPATH+=${PROJECT_ROOT}/arch/tm4c1294xl/port
VPATH+=${PROJECT_ROOT}/arch/tm4c1294xl/
VPATH+=${PROJECT_ROOT}/arch/tm4c1294xl/driverlib
VPATH+=${FREERTOS_ROOT}/portable/GCC/ARM_CM4F

${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/canbus_port.o

endif

ifeq (${PART}, CC3200)
include ${PROJECT_ROOT}/arch/cc3200-sdk/makedefs
IPATH+=${PROJECT_ROOT}/arch/cc3200-sdk/
IPATH+=${PROJECT_ROOT}/arch/cc3200-sdk/inc
IPATH+=${PROJECT_ROOT}/arch/cc3200-sdk/driverlib
IPATH+=${FREERTOS_ROOT}/portable/GCC/ARM_CM4

VPATH+=${PROJECT_ROOT}/arch/cc3200-sdk/port
VPATH+=${PROJECT_ROOT}/arch/cc3200-sdk/
VPATH+=${PROJECT_ROOT}/arch/cc3200-sdk/driverlib
VPATH+=${FREERTOS_ROOT}/portable/GCC/ARM_CM3
endif


VPATH+=${PROJECT_ROOT}/apps
VPATH+=${PROJECT_ROOT}/apps/eurobot
VPATH+=${PROJECT_ROOT}/apps/eurobot/jobs
VPATH+=${PROJECT_ROOT}/apps/eurobot/robot

# Path to project sources - include 'src' directory in project directory tree

VPATH+=${PROJECT_ROOT}/drivers/gpio
VPATH+=${PROJECT_ROOT}/drivers/uart
VPATH+=${PROJECT_ROOT}/drivers/canbus
VPATH+=${PROJECT_ROOT}/drivers/hooks
VPATH+=${PROJECT_ROOT}/drivers/logger
VPATH+=${PROJECT_ROOT}/drivers/timer
VPATH+=${PROJECT_ROOT}/apps
VPATH+=${PROJECT_ROOT}/tools/src/evergreen
VPATH+=${PROJECT_ROOT}
VPATH+=${FREERTOS_ROOT}
VPATH+=${FREERTOS_ROOT}/portable/MemMang


IPATH+=${PROJECT_ROOT}
IPATH+=${PROJECT_ROOT}/apps
IPATH+=${PROJECT_ROOT}/config
IPATH+=${PROJECT_ROOT}/tools/inc

# Include of FreeRTOS

IPATH+=${FREERTOS_ROOT}/include
IPATH+=${FREERTOS_ROOT}

CFLAGS+=-DSL_PLATFORM_MULTI_THREADED
CFLAGS+=-DUSE_FREERTOS
CFLAGS+=-DDEBUG




# The rule to clean out all the build products.
clean:
	@rm -rf ${OUTDIR} ${wildcard *~}


JOBS_SRC_FILES=$(wildcard ${PROJECT_ROOT}/apps/eurobot/jobs/*.c)
JOBS_PATHS=$(patsubst %.c,%.o,$(JOBS_SRC_FILES))
JOBS_O=$(notdir ${JOBS_PATHS})
JOBS=$(addprefix ${OUTDIR}/,${JOBS_O})
print:
	@echo ${JOBS}
# Rules for building
# FreeRTOS:
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/croutine.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/queue.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/tasks.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/port.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/heap_4.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/list.o


# Project:
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/init_port.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/gpio_port.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/uart_port.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/udma_port.o

${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/gpiolib.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/timerlib.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/uartlib.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/canbuslib.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/hooks.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/logger.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/tinyprintf.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/string_utils.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/atax.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/canbus_gateway.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/odometry.o
#${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/test.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/job.o
#${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/ping.o
${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/boot.o
${OUTDIR}/${TARGET_NAME}.axf: ${JOBS}


${OUTDIR}/${TARGET_NAME}.axf: ${OUTDIR}/startup_gcc.o

ifeq (${PART}, CC3200)
${OUTDIR}/${TARGET_NAME}.axf: ${PROJECT_ROOT}/arch/cc3200-sdk/driverlib/gcc/exe/libdriver.a
# LD rules:
SCATTERgcc_${TARGET_NAME}=${PROJECT_ROOT}/arch/cc3200-sdk/port/${TARGET_NAME}.ld
ENTRY_${TARGET_NAME}=ResetISR
endif

ifeq (${PART}, TM4C1294NCPDT)
${OUTDIR}/${TARGET_NAME}.axf: ${PROJECT_ROOT}/arch/tm4c1294xl/driverlib/gcc/libdriver.a
# LD rules:
SCATTERgcc_${TARGET_NAME}=${PROJECT_ROOT}/arch/tm4c1294xl/${TARGET_NAME}.ld
ENTRY_${TARGET_NAME}=ResetISR
endif

# Include the automatically generated dependency files.
ifneq (${MAKECMDGOALS},clean)
-include ${wildcard ${OUTDIR}/*.d} __dummy__
endif
