import sys
sys.path.insert(0, './pyserial')
from comm import ComPort


port = ComPort(input('Enter tty>> '), 115200)
port.open()
log = open('log', 'w')

try:
    while True:
        data = port.readString()
        if data is None:
            continue
        log.write(data + '\n')
        print(data, end = '\n')
        sys.stdout.flush()
except:
    log.close()
    print('Python script exiting...\n')
